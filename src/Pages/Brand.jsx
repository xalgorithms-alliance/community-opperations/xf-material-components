import React from 'react'
import './pageStyle/components.css'
import Container from '../Layouts/Container'
import { Button } from '../components'

function Brand () {
  const space = {
    fontFamily: 'spaceMono'
  }

  const flex = {
    width: '100%',
    display: 'grid',
    gridTemplateColumns: '25% 25% 25% 25%'
  }

  const slate = {
    width: '100%',
    height: '30px',
    background: 'var(--slate)',
    padding: '1em',
    color: 'var(--clear)'
  }

  const steel = {
    width: '100%',
    height: '30px',
    background: 'var(--steel)',
    padding: '1em',
    color: 'var(--clear)'
  }

  const frame = {
    width: '100%',
    height: '30px',
    background: 'var(--frame)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const white = {
    width: '100%',
    height: '30px',
    background: '#fff',
    padding: '1em',
    color: 'var(--steel)'
  }

  const naval = {
    width: '100%',
    height: '100px',
    background: 'var(--naval)',
    padding: '1em',
    color: 'var(--clear)'
  }

  const primary = {
    width: '100%',
    height: '100px',
    background: 'var(--primary)',
    padding: '1em',
    color: 'var(--clear)'
  }

  const firmament = {
    width: '100%',
    height: '100px',
    background: 'var(--firmament)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const clear = {
    width: '100%',
    height: '100px',
    background: 'var(--clear)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const royal = {
    width: '100%',
    height: '30px',
    background: 'var(--royal)',
    padding: '1em',
    color: 'var(--clear)'
  }

  const orchid = {
    width: '100%',
    height: '30px',
    background: 'var(--orchid)',
    padding: '1em',
    color: 'var(--clear)'
  }

  const satin = {
    width: '100%',
    height: '30px',
    background: 'var(--satin)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const gleam = {
    width: '100%',
    height: '30px',
    background: 'var(--gleam)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const error = {
    width: '100%',
    height: '30px',
    background: 'var(--error)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const coral = {
    width: '100%',
    height: '30px',
    background: 'var(--coral)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const valentine = {
    width: '100%',
    height: '30px',
    background: 'var(--valentine)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const smartie = {
    width: '100%',
    height: '30px',
    background: 'var(--smartie)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const groto = {
    width: '100%',
    height: '30px',
    background: 'var(--groto)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const clover = {
    width: '100%',
    height: '30px',
    background: 'var(--clover)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const lichen = {
    width: '100%',
    height: '30px',
    background: 'var(--lichen)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const seafoam = {
    width: '100%',
    height: '30px',
    background: 'var(--seafoam)',
    padding: '1em',
    color: 'var(--steel)'
  }

  const lockup = {
    maxWidth: '480px'
  }

  const mark = {
    maxWidth: '172px',
    margin: '1em'
  }

  const light = {
    width: '62px',
    margin: '1em'
  }

  const dark = {
    background: 'var(--slate)'
  }

  const dlhold = {
    marginTop: '2em',
    flexDirection: 'column'
  }

  const gret = {
    color: 'var(--steel)',
    fontWeight: '400'
  }

  return (
    <Container>
       <div className="head">
        <h3>The Visual Identity.</h3>
        <h4 style={gret}>Visual elements that, when fit together, form a system that is the brand.</h4>
        <div className="simpleFlex">
        </div>
      </div>
       <div className="head">
         <h1>Logo Mark</h1>
         <img src="lockup.png" style={lockup}/>
         <div style={dlhold}>
           <h3>Lockup</h3>
            <img src="Xalgorithms_Logo+LetterMakrk.png" style={mark}/>
            <br />
            <a href="Xalgorithms_Logo+LetterMakrk.png" download><Button>Download Lockup</Button></a>
         </div>
         <div style={dlhold}>
         <h3>Logomark</h3>
          <img src="LogoMark-Light.png" style={light}/>
          <br />
          <a href="LogoMark-Light.png" download><Button>Download Logomark</Button></a>
         </div>
         <div style={dlhold}>
          <h3>Logomark Dark Background</h3>
          <div style={dark}>
            <img src="LogoMark-Dark.png" style={light}/>
          </div>
          <br />
          <a href="LogoMark-Dark.png" download><Button>Download Logomark</Button></a>
         </div>
       </div>
      <div className="head">
        <h1>Color</h1>
        <div style={flex}>
        <div style={naval}></div>
        <div style={primary}></div>
        <div style={firmament}></div>
        <div style={clear}></div>
    </div>
    <div style={flex}>
        <p className="formSmall">Naval #062AA7</p>
        <p className="formSmall">Primary #204EF0</p>
        <p className="formSmall">Firmament #DBEAFF</p>
        <p className="formSmall">Clear #EDF5FF</p>
    </div>
    <div style={flex}>
        <div style={slate}></div>
        <div style={steel}></div>
        <div style={frame}></div>
        <div style={white}></div>
    </div>
    <div style={flex}>
        <p className="formSmall">Slate #1E2033</p>
        <p className="formSmall">Steel #39435B</p>
        <p className="formSmall">Frame #E7EAEE</p>
        <p className="formSmall">White #FFF</p>
    </div>
    <div style={flex}>
        <div style={royal}></div>
        <div style={orchid}></div>
        <div style={satin}></div>
        <div style={gleam}></div>
    </div>
    <div style={flex}>
        <p className="formSmall">Royal #372989</p>
        <p className="formSmall">Orchid #DBD7F8</p>
        <p className="formSmall">Satin #F1EEFC</p>
        <p className="formSmall">Gleam #F9FBFE</p>
    </div>
    <div style={flex}>
        <div style={error}></div>
        <div style={coral}></div>
        <div style={valentine}></div>
        <div style={smartie}></div>
    </div>
    <div style={flex}>
        <p className="formSmall">Error #B44C3E</p>
        <p className="formSmall">Coral #ED9C91</p>
        <p className="formSmall">Valentine #F6CDC7</p>
        <p className="formSmall">Smartie #FAE5E2</p>
    </div>
    <div style={flex}>
        <div style={groto}></div>
        <div style={clover}></div>
        <div style={lichen}></div>
        <div style={seafoam}></div>
    </div>
    <div style={flex}>
        <p className="formSmall">Groto #6C8F73</p>
        <p className="formSmall">Clover #9CD1A8</p>
        <p className="formSmall">Lichen #BFDFC6</p>
        <p className="formSmall">Seafoam #E4F2E7</p>
    </div>
      </div>
      <div className="head">
        <h1>Type</h1>
        <h3>Public Sans</h3>
        <h2 className="baskervilleHeadline">Libre Baskerville</h2>
        <h3 style={space}>Space Mono</h3>
      </div>
    </Container>
  )
}

export default Brand
