import React from 'react'
import { Button, LinkButton, CodeArea, InputDate, InputText, TextArea, InputCheck, InputSlider, InputDropdown, Icon, Column, Grid12 } from '../components'
import Container from '../Layouts/Container'
import './pageStyle/components.css'

const Components = () => {
  const gret = {
    color: 'var(--steel)',
    fontWeight: '400'
  }

  const testBox = {
    width: '100%',
    height: '100px',
    border: '1px solid white',
    background: 'var(--primary)'
  }
  // eslint-disable-next-line quotes
  const install = `$ yarn add xf-material-components`

  const reactstyle = `// App.js

import { Button, InputText, InputDate } from 'xf-material-components'
import GlobalStyle from 'xf-material-components/config/global.styles'
  
function App() {
  return (
    <div className="App">
      <GlobalStyle />
      <header className="App-header">
        <Button>Hello World</Button>
        <InputText />
        <InputDate />
      </header>
    </div>
  );
}`

  const nextjs = `// index.js

  import 'xf-material-components/config/rootStyle.css'`

  const iconBlock = `<Icon name="callendar"/>
<Icon name="ArrowRight" />
<Icon name="ArrowLeft" />
<Icon name="Upload" />
<Icon name="Download" />
<Icon name="Reset" />
<Icon name="Trash" />
<Icon name="External" />
<Icon name="Add" />
<Icon name="Toggle" />
<Icon name="Info" />
<Icon name="Settings" />
<Icon name="Standards" />
<Icon name="Edit" />`

  const textinputBlock = `import React from 'react'
import { InputText } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputText 
      value={yourString} 
      placeHolder={yourString} 
      onChange={yourFunction} 
      errorMessage={yourString}
    />
  )
}

export default YourFunction`

  const textareaBlock = `import React from 'react'
import { TextArea } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputText 
      value={yourString} 
      placeHolder={yourString} 
      onChange={yourFunction} 
      errorMessage={yourString}
    />
  )
}

export default YourFunction`

  const checkBlock = `// need to add logic
import React from 'react'
import { InputCheck } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputCheck />
  )
}

export default YourFunction`

  const sliderBlock = `import React from 'react'
import { InputSlider } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputSlider onChange={yourFunction} />
  )
}

export default YourFunction`

  const dropdownBlock = `import React from 'react'
import { InputDropdown } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputDropdown
      onChange={yourFunction}
      options={[
        { value: 'yourValue', label: 'yourLabel' }
      ]} 
     />
  )
}

export default YourFunction`

  const dateBlock = `import React from 'react'
import { InputDate } from 'xf-material-components'

const YourFunction = () => {
  return (
    <InputDate 
      value={yourString} 
      placeHolder={yourString} 
      onChange={yourFunction} 
    />
  )
}

export default YourFunction`

  const type = `// paragraph
  
<p>Text<p>
<p className="baskerville">Text<p>

// inline elements

<a>Link</a>
<i>Italic</i>
<p className="baskerville"><i>Italic</i></p>
<code>inline code</code>
<mark>highlight</mark>`

  const h = `<h1>Rules as data</h1>
<h2>Rules as data</h2>
<h3>Rules as data</h3>
<h4>Rules as data</h4>
<h5>Rules as data</h5>`

  const block = `import React from 'react'
import { CodeArea } from 'xf-material-components'

const YourFunction = () => {
  const yourCode = ˴this is how you use a code block˴
  return (
    <CodeArea code={yourCode}/>
  )
}

export default YourFunction`

  const buttonBlock = `import React from 'react'
  import { Button } from 'xf-material-components'

  const YourFunction = () => {
    return (
      <Button>Hello World </Button>
      <Button type="clear">Hello World</Button>
      <Button type="disabled">Hello World</Button>
      <Button type="wide">Hello World</Button>  
    )
  }
  
  export default YourFunction`

  // eslint-disable-next-line quotes
  const serif = `<h2 className="baskervilleHeadline">Your text</h2>`
  return (
    <>
    <Container>
      <div className="head">
        <h3>The Component Language.</h3>
        <h4 style={gret}>Simple, modular React components that build Xalgorithms Foundation interfaces. </h4>
        <div className="simpleFlex">
          <a href="https://gitlab.com/xalgorithms-alliance/xf-material-components" target="blank">
            <Button>See the Code</Button>
          </a>
          <a href="#iconic">
            <Button type="clear">Read the Documentation</Button>
          </a>
        </div>
      </div>
    </Container>
    <div className="imgHold">
      <img className="cover" src="./components.jpg"/>
    </div>
    <Container>
      <div className="head">
        <h1>Installation</h1>
        <p>For installation, download the package into your project folder.</p>
        <CodeArea code={install}/>
        <p>For react projects import <code>GlobalStyle</code> into the root of your app. </p>
        <CodeArea code={reactstyle}/>
        <p>For react next.js projects, import the <code>rootStyle</code> into your index.js file.</p>
        <CodeArea code={nextjs}/>
      </div>
      <div className="head" id="iconic">
        <h1>Icons</h1>
      </div>
      <div className="head">
        <div className="simpleFlex">
        <Icon name="callendar"/>
        <Icon name="ArrowRight" />
        <Icon name="ArrowLeft" />
        <Icon name="Upload" />
        <Icon name="Download" />
        <Icon name="Reset" />
        <Icon name="Trash" />
        <Icon name="External" />
        <Icon name="Add" />
        <Icon name="Toggle" />
        <Icon name="Info" />
        <Icon name="Settings" />
        <Icon name="Standards" />
        <Icon name="Edit" />
        </div>
        <CodeArea code={ iconBlock }/>
      </div>
      <div className="head">
        <h1>Type</h1>
      </div>
      <div className="head">
      <h4 style={gret}>
        Titles
      </h4>
        <div className="fontFlex">
          <p className="formSmall">h1</p>
          <h1>Rules as data</h1>
        </div>
        <div className="fontFlex">
          <p className="formSmall">h2</p>
          <h2>Rules as data</h2>
        </div>
        <div className="fontFlex">
          <p className="formSmall">h3</p>
          <h3>Rules as data</h3>
        </div>
        <div className="fontFlex">
          <p className="formSmall">h4</p>
          <h4>Rules as data</h4>
        </div>
        <div className="fontFlex">
          <p className="formSmall">h5</p>
          <h5>Rules as data</h5>
        </div>
        <CodeArea code={ h }/>
      </div>
      <div className="head">
        <h4 style={gret}>
          Serif Contrast
        </h4>
        <h2 className="baskervilleHeadline">Lorem ipsum dolor sit amet, consectetur adipiscing elit ut aliquam, purus sit</h2>
        <CodeArea code={ serif }/>
      </div>
      <div className="head">
        <h4 style={gret}>
          Paragraph
        </h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ridiculus tempor, dignissim lectus justo, etiam condimentum. </p>
        <p className="baskerville">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ridiculus tempor, dignissim lectus justo, etiam condimentum. </p>
        <div className="simpleFlex">
          <a>Link</a>
          <p><i>Italic</i></p>
          <p className="baskerville"><i>Italic</i></p>
          <code>inline code</code>
          <mark>highlight</mark>
        </div>
        <CodeArea code={ type }/>
      </div>
      <div className="head">
        <h4 style={gret}>
          Code Block
        </h4>
        <CodeArea code={ block }/>
      </div>
      <div className="head">
        <h1>Buttons</h1>
      </div>
      <div className="head">
        <h4 style={gret}>
          Buttons
        </h4>
        <div className="simpleFlex">
          <Button>Hello World </Button>
          <Button type="clear">Hello World</Button>
          <Button type="disabled">Hello World</Button>
        </div>
        <Button type="wide">Hello World</Button>
        < LinkButton label="Hello World" target="xalgorithms.org" />
        <CodeArea code={ buttonBlock }/>
      </div>
      {/*
      <div className="head">
        <h4 style={gret}>
          Buttons
        </h4>
        <div className="simpleFlex">
          <Tabs label="test" active="true" />
        </div>
        <CodeArea code={ buttonBlock }/>
      </div>
  */}
    <div className="head">
      <h1>Inputs</h1>
    </div>
    <div className="head">
    <h4 style={gret}>
        Input Date
    </h4>
    </div>
    <div className="head">
    <InputDate />
    <CodeArea code={ dateBlock }/>
    </div>
    <div className="head">
    <h4 style={gret}>
        Input Text
    </h4>
    <InputText />
    <InputText errorMessage="An error"/>
    <CodeArea code={ textinputBlock }/>
    </div>
    <div className="head">
    <h4 style={gret}>
        Text Area
    </h4>
    <TextArea />
    <TextArea errorMessage="An Error"/>
    <CodeArea code={ textareaBlock }/>
    </div>
    <div className="head">
    <h4 style={gret}>
        Check
    </h4>
    <InputCheck />
    <CodeArea code={ checkBlock }/>
    </div>
    <div className="head">
    <h4 style={gret}>
        Slider
    </h4>
    <InputSlider />
    <CodeArea code={ sliderBlock }/>
    </div>
    <div className="head">
    <h4 style={gret}>
        Dropdown
    </h4>
    <InputDropdown
    options={[
      { value: 'test', label: 'Test' },
      { value: 'test', label: 'Test' }
    ]}/>
    <CodeArea code={ dropdownBlock }/>
    </div>
    <div className="head">
      <h1>Layout</h1>
    </div>
    <div className="head">
      <h4 style={gret}>
          Grid
      </h4>
      <Grid12>
       <div style={testBox} />
       <div style={testBox} />
       <div style={testBox} />
       <div style={testBox} />
       <div style={testBox} />
       <div style={testBox} />
       <div style={testBox} />
       <div style={testBox} />
       <div style={testBox} />
       <div style={testBox} />
       <div style={testBox} />
       <div style={testBox} />
      </Grid12>
    </div>
    <div className="head">
      <h4 style={gret}>
          Column
      </h4>
      <Column>
        <div style={testBox} />
        <div style={testBox} />
      </Column>
    </div>
  </Container>
  </>
  )
}

export default Components
