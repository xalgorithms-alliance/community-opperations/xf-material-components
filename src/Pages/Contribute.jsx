import React from 'react'
import ReactMarkdown from 'react-markdown'
import ContributionGuidelines from '../Content/ContributionGuidelines'
import Container from '../Layouts/Container'
import './pageStyle/components.css'
import { Button } from '../components'

function Contribute () {
  const gret = {
    color: 'var(--steel)',
    fontWeight: '400'
  }
  return (
    <Container>
       <div className="head">
        <h3>Building in public. By the public.</h3>
        <h4 style={gret}>See what&apos;s in the pipeline, and where you can lend a hand. </h4>
        <p>
          📎 This page accompanies the <a href="https://development.xalgorithms.org/roadmaps/2020.spring/" target="blank">technical roadmap</a>.
        </p>
        <div className="simpleFlex">
          <a href="https://gitlab.com/xalgorithms-alliance/xf-material-components/-/milestones/1" target="blank">
            <Button>Project Overview</Button>
          </a>
        </div>
      </div>
      <div className="head">
      <ReactMarkdown>{ ContributionGuidelines }</ReactMarkdown>
      </div>
    </Container>
  )
}

export default Contribute
