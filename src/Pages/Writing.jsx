import React from 'react'
import ReactMarkdown from 'react-markdown'
import WritingGuidelines from '../Content/WritingGuidelines'
import Container from '../Layouts/Container'

function Writing () {
  return (
    <Container>
      <div className="head">
        <ReactMarkdown>
          { WritingGuidelines }
        </ReactMarkdown>
      </div>
    </Container>
  )
}

export default Writing
