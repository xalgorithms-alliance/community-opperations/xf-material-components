import React from 'react'
import GlobalStyle from './config/global.styles'

// eslint-disable-next-line react/prop-types
export default function Provider ({ children }) {
  return <><GlobalStyle />{children}</>
}
