/* eslint-disable camelcase */
import React from 'react'
import PropTypes from 'prop-types'
import * as iconlib from './iconlib'

const default_size = 22
const default_viewbox = 22

const Icon = ({
  name,
  fill = 'var(--steel)',
  size = default_size,
  size_y = default_size,
  x = default_viewbox,
  y = default_viewbox
}) => {
  const paths = iconlib[name]
  const viewbox = ('0 0 ' + x + ' ' + y) // Pass paths to this to finish the function.

  if (!paths) {
    throw new Error('No paths given to icon.')
  }
  /*
  useEffect(() => {
  })
  */

  const renderIcon = () => paths.map((pathContent, i) => {
    return <path key={i} d={pathContent.d} fill={fill} />
  })

  // Use border for development:
  // style={{ border: '1px solid #EEE', borderRadius: '4px' }}
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={`${size}px`}
      height={`${size_y === default_size ? size : size_y}px`}
      viewBox={viewbox}
    >
      <title>{`icon-${name}`}</title>
      {renderIcon()}
    </svg>
  )
}

Icon.propTypes = {
  name: PropTypes.string,
  fill: PropTypes.string,
  size: PropTypes.number,
  size_y: PropTypes.number,
  x: PropTypes.number,
  y: PropTypes.number
}

export default Icon
