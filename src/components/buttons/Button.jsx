import React, { useRef, useEffect } from 'react'
import PropTypes from 'prop-types'

function Button ({ children, type, onClick }) {
  const invisible = {
    background: 'none',
    border: 'none',
    outline: 'none',
    cursor: 'pointer',
    hover: {
      background: 'none',
      border: 'none',
      outline: 'none',
      cursor: 'pointer'
    }
  }

  const standard = {
    background: 'var(--primary75)',
    padding: '0.5em 2em 0.5em 2em',
    borderRadius: '0.3em',
    fontSize: '1em',
    outline: 'none',
    border: 'none',
    color: 'white',
    boxShadow: '3px 3px 4px #E7E7E7',
    cursor: 'pointer',
    transition: 'background .3s, border .3s, box-shadow .2s',
    hover: {
      background: 'var(--primary100)',
      padding: '0.5em 2em 0.5em 2em',
      borderRadius: '0.3em',
      fontSize: '1em',
      border: 'none',
      outline: 'none',
      color: 'white',
      boxShadow: '3px 3px 4px var(--frame)',
      cursor: 'pointer'
    }
  }

  const clear = {
    background: 'white',
    padding: '0.5em 2em 0.5em 2em',
    borderRadius: '0.3em',
    fontSize: '1em',
    border: 'none',
    outline: 'none',
    color: 'var(--primary75)',
    boxShadow: 'none',
    cursor: 'pointer',
    transition: 'background .3s, border .3s, box-shadow .2s',
    hover: {
      background: 'mono00',
      padding: '0.5em 2em 0.5em 2em',
      borderRadius: '0.3em',
      fontSize: '1em',
      border: 'none',
      outline: 'none',
      color: 'var(--naval)',
      boxShadow: 'none',
      cursor: 'pointer'
    }
  }

  const disabled = {
    background: 'mono00',
    padding: '0.5em 2em 0.5em 2em',
    borderRadius: '0.3em',
    fontSize: '1em',
    border: 'none',
    outline: 'none',
    color: 'var(--frame)',
    boxShadow: '3px 3px 4px #E7E7E7',
    cursor: 'pointer',
    transition: 'background .3s, border .3s, box-shadow .2s',
    hover: {
      background: 'white',
      padding: '0.5em 2em 0.5em 2em',
      borderRadius: '0.3em',
      fontSize: '1em',
      border: 'none',
      outline: 'none',
      color: 'var(--frame)',
      boxShadow: '4px 4px 4px #E7E7E7',
      cursor: 'pointer'
    }
  }

  const wide = {
    background: 'var(--primary75)',
    padding: '0.5em 2em 0.5em 2em',
    borderRadius: '0.3em',
    fontSize: '1em',
    border: 'none',
    outline: 'none',
    color: 'white',
    boxShadow: '3px 3px 4px #E7E7E7',
    width: '100%',
    cursor: 'pointer',
    transition: 'background .3s, border .3s, box-shadow .2s',
    hover: {
      background: 'var(--primary100)',
      padding: '0.5em 2em 0.5em 2em',
      borderRadius: '0.3em',
      fontSize: '1em',
      border: 'none',
      outline: 'none',
      color: 'white',
      boxShadow: '3px 3px 4px #E7E7E7',
      width: '100%',
      cursor: 'pointer'
    }
  }

  const [buttonType, setButtonType] = React.useState(standard)
  /*
  const prevCountRef = useRef()
  useEffect(() => {
    prevCountRef.current = buttonType
  })
  const prevCount = prevCountRef.current
  */

  function toggleHover () {
    setButtonType(buttonType.hover)
  }

  function toggleLeave () {
    if (type === 'invisible') {
      setButtonType(invisible)
    } else if (type === 'wide') {
      setButtonType(wide)
    } else if (type === 'clear') {
      setButtonType(clear)
    } else if (type === 'disabled') {
      setButtonType(disabled)
    } else {
      setButtonType(standard)
    }
  }

  function pickStyle (ref) {
    useEffect(() => {
      if (type === 'invisible') {
        setButtonType(invisible)
      } else if (type === 'wide') {
        setButtonType(wide)
      } else if (type === 'clear') {
        setButtonType(clear)
      } else if (type === 'disabled') {
        setButtonType(disabled)
      } else {
        setButtonType(standard)
      }
    }, [ref])
  }

  const wrapperRef = useRef(null)
  pickStyle(wrapperRef)

  return (
    <div ref={wrapperRef}>
        <button onClick={onClick} style={buttonType} onMouseEnter={toggleHover} onMouseLeave={toggleLeave}>{children}</button>
    </div>
  )
}

Button.propTypes = {
  children: PropTypes.any,
  type: PropTypes.string,
  onClick: PropTypes.func
}

export default Button
