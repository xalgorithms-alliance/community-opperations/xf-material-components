import React from 'react'
import Icon from '../Icons/Icon.jsx'
import PropTypes from 'prop-types'

function LinkButton ({ label, target }) {
  const linkText = {
    color: 'var(--primary)',
    textDecoration: 'none'
  }

  return (
        <a href={target} style={linkText}>
            <div className="linkButtonHold">
                <p style={linkText}>{label}</p>
                <Icon name="ArrowRight" fill='var(--primary)' />
            </div>
        </a>
  )
}

LinkButton.propTypes = {
  label: PropTypes.string,
  target: PropTypes.string
}

export default LinkButton
