import React, { useRef, useEffect } from 'react'
import PropTypes from 'prop-types'
import Button from './Button.jsx'

function Tabs ({ label, active }) {
  const primary = {
    color: 'var(--primary)',
    transition: 'color .3s'
  }

  const frame = {
    color: 'var(--steel)',
    transition: 'color .3s'
  }

  const HoldPrimary = {
    padding: '1em 1em 1em 0',
    width: '100%',
    borderBottom: '1px solid var(--primary)',
    transition: 'border .3s'
  }

  const HoldFrame = {
    padding: '1em 1em 1em 0',
    width: '100%',
    borderBottom: '1px solid var(--frame)',
    transition: 'border .3s'
  }

  const [style, setStyle] = React.useState(frame)
  const [hold, setHold] = React.useState(HoldFrame)

  const prevCountRef = useRef()
  useEffect(() => {
    prevCountRef.current = style
  })
  const prevCount = prevCountRef.current

  function toggleHover () {
    setStyle(primary)
    setHold(HoldPrimary)
  }

  function toggleLeave () {
    setStyle(prevCount)
    setHold(HoldFrame)
  }

  function setBorder (ref) {
    useEffect(() => {
      if (active) {
        setStyle(primary)
        setHold(HoldFrame)
      } else {
        setStyle(frame)
        setHold(HoldFrame)
      }
    }, [ref])
  }
  const wrapperRef = useRef(null)
  setBorder(wrapperRef)

  return (
    <div
    ref={wrapperRef}
    onMouseEnter={toggleHover}
    onMouseLeave={toggleLeave}
  >
    <div style={hold}>
            <Button type="invisible">
              <p style={style}>
                {label}
              </p>
            </Button>
    </div>
  </div>
  )
}

Tabs.propTypes = {
  label: PropTypes.string,
  active: PropTypes.bool
}

export default Tabs
