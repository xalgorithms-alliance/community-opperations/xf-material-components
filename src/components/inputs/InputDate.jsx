import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const InputTxt = styled.input`
    -webkit-appearance: none;
    -moz-appearance: 'none';
    border: 1px solid;
    border-color: var(--frame);
    padding: 0.5em;
    border-radius: 0.5em;
    font-size: 1em;
    box-shadow: none;
    font-family: PublicSans;
    font-weight: 200;
    color: var(--steel);
    width: 100%;
    appearance: none;
    &:hover {
      -webkit-appearance: none;
      border: 1px solid var(--primary);
      outline: none;
    }
    :focus {
        outline: 0;
        border: 1px solid;
        border-color: var(--primary);
        border-radius: 0.5em;
        box-shadow: 0 0 0 2px var(--firmament);
    }
    &:active {
      -webkit-appearance: none;
      border: 1px solid var(--primary);
      outline: none;
      box-shadow: 0 0 0 2px var(--firmament);
    }
    ::-webkit-calendar-picker-indicator { background: url('public/i-calendar-01.svg') no-repeat right center; }
    `

function InputDate ({ value, placeholder, onChange }) {
  return (
        <InputTxt type="date"
            value={value}
            placeholder={placeholder}
            onChange={onChange}
        />
  )
}

InputDate.propTypes = {
  value: PropTypes.string,
  placeholder: PropTypes.string,
  onChange: PropTypes.func,
  color: PropTypes.any
}

export default InputDate
