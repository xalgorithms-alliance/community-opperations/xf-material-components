import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Slider = styled.input`
    -webkit-appearance: none;
    width: 100%;
    height: 1em;
    border-radius: 0.25em;
    padding: 0;
    background: #fff;
    border: 1px solid var(--frame);
    transition: background .3s, border .3s, box-shadow .2s;
    &::-webkit-slider-thumb {
      -webkit-appearance: none;
      width: 2px;
      height: 1em;
      borderRadius: 8px;
      background-color: var(--primary);
      cursor: pointer;
    }
    &::-moz-range-thumb {
      -webkit-appearance: none;
      appearance: none;
      border: none;
      width: 2px;
      height: 1em;
      background-color: var(--primary);
      cursor: pointer;
    }
    &:hover {
        -webkit-appearance: none;
        border: 1px solid var(--primary);
        outline: none;
    }
    &:active {
        -webkit-appearance: none;
        border: 1px solid var(--primary);
        outline: none;
    }
    &:focus {
        -webkit-appearance: none;
        border: 1px solid var(--primary);
        outline: none;
        box-shadow: 0 0 0 2px var(--firmament);
    }
  `

function InputSlider ({ onChange }) {
  return (
        <Slider
            type="range"
            onChange={onChange}
        />
  )
}

InputSlider.propTypes = {
  onChange: PropTypes.func
}

export default InputSlider
