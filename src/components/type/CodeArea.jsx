import React from 'react'
import Editor from 'react-simple-code-editor'
import { highlight, languages } from 'prismjs'
import 'prismjs/components/prism-jsx'
import PropTypes from 'prop-types'

const codeHold = {
  background: 'var(--mono175)',
  padding: '1em',
  borderRadius: '0.5em'
}

function CodeArea ({ code }) {
  return (
      <div style={codeHold}>
        <Editor
            disabled={true}
            value={code}
            highlight={code => highlight(code, languages.jsx, 'react')}
            style={{
              fontFamily: '"SpaceMono", monospace',
              fontSize: '0.8em',
              fontVariantLigatures: 'none',
              background: 'var(--mono175)',
              color: 'var(--aqua50)'
            }}
        />
      </div>
  )
}

CodeArea.propTypes = {
  code: PropTypes.any
}

export default CodeArea
