```jsx
const test =
`<form onSubmit={this.handleSubmit}>
    <label>
        Essay:
        <textarea value={this.state.value} onChange={this.handleChange} />
    </label>
    <input type="submit" value="Submit" />
</form>`;

<CodeArea code={test}/>
```