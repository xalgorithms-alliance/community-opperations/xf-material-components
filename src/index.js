import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import './index.css'
import App from './App'
import Brand from './Pages/Brand'
import Writing from './Pages/Writing'
import Components from './Pages/Components'
import Contribute from './Pages/Contribute'
import Navbar from './Layouts/Navbar'
import './config/rootStyle.css'

const Routing = () => {
  return (
    <Router>
      <Navbar/>
      <Switch>
        <Route exact path="/" component={App} />
        <Route path="/brand" component={Brand} />
        <Route path="/writing" component={Writing} />
        <Route path="/components" component={Components} />
        <Route path="/contribute" component={Contribute} />
      </Switch>
    </Router>
  )
}

ReactDOM.render(
  <React.StrictMode>
    <Routing />
  </React.StrictMode>,
  document.getElementById('root')
)
